<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/a39{b7oQS|m;Ur=3>gMThW;XxT-,-W8,q$DCm>^;_(`>4urZuWS .go^h)Cby/7');
define('SECURE_AUTH_KEY',  'SV~uA}AV?`SmQ++4)w3E%ttplr39|Mo2!a[,%c8.=R_LhX5=-uJ,y?uh%$/Xd^=]');
define('LOGGED_IN_KEY',    '!3f1BPBT@e+3Nf?N)Zs_C9>.fGK,k4f|DFPX.F;]lV:B9~3ONl%h!;nUzwSlNsAJ');
define('NONCE_KEY',        'f28k k|0Fx~8vqIceCnBHQy?uCoH4@<]Q6[(se+`37g/235d[ . :s<X<H$((7s~');
define('AUTH_SALT',        'lsMd,,;aEDhC0SzW|u1 #xVnarrO5c[7dd-wL*Sn+3IP[?[FEC:PO6.c:t}Uib)l');
define('SECURE_AUTH_SALT', 'KINcgA%sqh@>tK`Zm =Eu<8)V$?l@2_yF.=zpDR.%ESywrG52Hb-L9)^/D<f{}l4');
define('LOGGED_IN_SALT',   'HF9q3M2 lhYMG!_5z5YL5JtR1^G8-CLn+bdL|{}8hgu/0a@et5c,9Hpf$F`W[0|k');
define('NONCE_SALT',       'JOmh<#$KH#H6JB^S[ pe.MC7ejxC6aE,!(FJKjO-um+}Dc%OecIP}gy!x,`T-ar{');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
